
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 4
echo $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

<? php 

// require('animal.php');
// require('motor.php');
require('ape.php')

$object = new Animal("shaun");
echo "Nama: $object->name "<br>"";
echo "Jumlah kaki: $object->legs"<br>"";
echo "jenis darah : $object->cold_blooded "<br>"";

$kodok = new frog("buduk");
echo "Nama : $kodok->name "<br>"";
echo "Jumlah kaki : $kodok->legs "<br>"";
echo "jenis darah: $kodok->cold_blooded "<br>"";
echo $kodok->jump();

$sungokong = new ape("kera sakti");
echo "Nama: $sungokong->name "<br>"";
echo "Jumlah kaki : $sungokong->legs "<br>"";
echo "jenis darah: $sungokong->cold_blooded "<br>"";
echo $sungokong->yell();

